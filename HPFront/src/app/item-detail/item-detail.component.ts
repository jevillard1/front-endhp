import { Component, OnInit, Input } from '@angular/core';
import { Item } from 'src/Classes/Item';
import { ActivatedRoute } from '@angular/router';
import { ItemService } from '../services/item.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {

  @Input() item: Item;

  constructor(
    private route: ActivatedRoute,
    private location:Location,
    private itemService: ItemService
  ) { }

  ngOnInit() {
    this.getItem();
  }
  getItem():void{//Appel le back pour avoir les informations sur l'item dans l'input.
    const id = +this.route.snapshot.paramMap.get('id');
    this.itemService.getItem(id)
      .subscribe(item => this.item = item);
  }

  goBack():void{//Permet de revenir à la page de base. Ceci est associé à un bouton.
    this.location.back();
  }

}
