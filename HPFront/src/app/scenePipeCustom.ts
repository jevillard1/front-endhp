import { Pipe } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';


@Pipe({name: 'safeHtml'})
export class Safe {
  constructor(private sanitizer:DomSanitizer){}

  transform(style) {
     var truc = this.sanitizer.bypassSecurityTrustHtml(style);
     console.log(truc);
     return truc;
    //return this.sanitizer.bypassSecurityTrustStyle(style);
    //return this.sanitizer.bypassSecurityTrustUrl(style); - see docs
  }
}