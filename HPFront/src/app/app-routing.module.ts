import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharacterDetailComponent } from './character-detail/character-detail.component';
import { CharacterComponent } from './character/character.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';



const routes: Routes = [
  {path:'', redirectTo:'/player', pathMatch: 'full'},
  {path:'player', component: CharacterComponent},
  {path:'characterdetail/:id',component: CharacterDetailComponent},
  {path:'item/:id', component: ItemDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations:[]
})
export class AppRoutingModule { }
