import { Component, OnInit } from '@angular/core';
import { CharacterService } from '../services/character.service';
import { Character } from './../../Classes/Character';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css'],
})
export class CharacterComponent implements OnInit {

  player : Character; //Variable contenant les informations sur le joueur.
  team : Character[];//Variable contenant la liste des personnages que le joueur possède dans ça team.
  selectCharacter: Character;//Variable utilisé pour l'affichage des détails d'un personnages dans la team.
  
  // isOpen = true;//Variable d'état utilisé pendant les testes 
  // toggle():void{
  //   this.isOpen = !this.isOpen
  // }
  constructor(
    private characterService: CharacterService
  ) {}
  
  onSelect(character: Character):void{//Permet de séléctionner un personnage de la team. Associé à un badge dans le code HTML 
    this.selectCharacter = character;
  }
  unSelect():void{//Permet de plus séléctionner le personnage lorsque il était séléctionner. Associé à un bouton dans le code HTML
    this.selectCharacter = null;
  }

  getPlayer():void{//Appel le back pour faire un get sur les informations du joueur.
    this.characterService.getPlayer()
    .subscribe(player => this.player = player);
  }

  getTeam():void{//Appel le back pour faire un get sur les informations concernant les personnage que contrôle le joueurs
    this.characterService.getCharacters()
    .subscribe(team => this.team = team);
  }

  ngOnInit() {
    this.getPlayer();
    this.getTeam();
  }
  
}
