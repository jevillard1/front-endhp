import { Component, OnInit, Input,  ElementRef, Renderer2, ViewChild } from '@angular/core';
import { Scene } from 'src/Classes/Scene';
import { Action } from '../../Classes/Action';
import { SceneService } from '../services/scene.service';

@Component({
  selector: 'app-scene',
  templateUrl: './scene.component.html',
  styleUrls: ['./scene.component.css']
})
export class SceneComponent implements OnInit {

  
  @Input() scene: Scene;

  constructor(
    private sceneService: SceneService
    ) {}


  ngOnInit(): void {
    this.getScene("1");
  }
  
  getScene(nomAction): void {
    this.sceneService.getScene(nomAction)
      .subscribe(scene => { this.scene = scene; this.parse(this.scene);} );
    
  }

  onChangeScene(){    
    let nomAction = document.getElementById("save").getAttribute("data-action");
    this.getScene(nomAction); 
  }   

  parse(scene)
  {
      var reg = /\{[a-zA-Z0-9_-]+\s+([^}]+)\}/;
      var reg2 = /\s+([^}]+)\}/;
      var IndexDeb = 0;
      var IndexFin = 0;
      var IndexMot = 0;
      var action = "";
      var Mot = "";
      var NomAction  = "";
      var but = "";
      var i = 0;
      
      var content = scene.Content;

      let actionNames = [];
      while(content.search(reg) != -1)
      {
          console.log(content);
          
          IndexDeb = content.search(reg);
          IndexFin = content.indexOf("}");

          action = content.substring(IndexDeb,IndexFin+1);

          IndexMot = action.search(reg2);
          NomAction = action.substring(1,IndexMot);
          console.log(NomAction);
          Mot = action.substring(IndexMot+1,action.length-1);

          console.log(Mot); 
          //scene.listOfAction.push(new Action(0,Mot));
          console.log('La je push');
          but = `<a style="color: blue; cursor: pointer;" id="button_${NomAction}" onclick="let a=document.getElementById('save');a.setAttribute('data-action','${NomAction}');a.dispatchEvent(new MouseEvent('click'));">${Mot}</a>`
          content = content.replace(reg, but);
      } 
      
      scene.Content = "<div>" + content + "</div>";
  }

}
