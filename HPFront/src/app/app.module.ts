import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule, Pipe } from '@angular/core';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { HttpClientModule }    from '@angular/common/http';
import {InMemoryDataService} from './services/in-memory-data.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SceneComponent } from './scene/scene.component';
import { SceneTextComponent } from './scene-text/scene-text.component';
import { Safe } from './scenePipeCustom';
import { CharacterDetailComponent } from './character-detail/character-detail.component';
import { CharacterComponent } from './character/character.component';
import { InventoryComponent } from './inventory/inventory.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    SceneComponent,
    SceneTextComponent,
    Safe,
    CharacterDetailComponent,
    CharacterComponent,
    InventoryComponent,
    ItemDetailComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    // HttpClientInMemoryWebApiModule.forRoot(
    //   InMemoryDataService,{ dataEncapsulation: false}
    //  ),
    AppRoutingModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

