import { Component } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('myInventory', [
      state('true', style({width: '80%', color:'black'})),
      state('false', style({ opacity: 0, width: '0px' })),
      transition('false <=> true', animate(500))
    ]),
  ] 
})
export class AppComponent {
  title = 'HPFront';
  color = 'yellow';
  isShown = false;

  toggle():void{
    this.isShown = !this.isShown;
    console.log(this.isShown);
  }
}
