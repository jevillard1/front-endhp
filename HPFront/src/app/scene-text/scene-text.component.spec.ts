import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SceneTextComponent } from './scene-text.component';

describe('SceneTextComponent', () => {
  let component: SceneTextComponent;
  let fixture: ComponentFixture<SceneTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SceneTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SceneTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
