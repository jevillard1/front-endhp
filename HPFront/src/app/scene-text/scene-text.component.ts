import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-scene-text',
  templateUrl: './scene-text.component.html',
  styleUrls: ['./scene-text.component.css']
})
export class SceneTextComponent implements OnInit {

  @Input() contentScene: string = '<button> this is header </button>'; 

  constructor() { }

  ngOnInit() {
  }

}
