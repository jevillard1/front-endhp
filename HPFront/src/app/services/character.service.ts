import { Injectable } from '@angular/core';
import { Character } from '../../Classes/Character';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {
  private characterUrl = "api/characters";//appel l'api pour la team.
  private playerUrl= "api/player"; //appel l'api pour le Joueur.

  private handleError<T>(operation = 'operation', result?: T){
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    }
  }
  constructor(
    private http: HttpClient
  ) { }

  getCharacters():Observable<Character[]>{//Permet de faire get d'une liste de character du back pour le CharacterComponent. Sert à initialiser pour la team du joueur. 
    return this.http.get<Character[]>(this.characterUrl)
    .pipe(
      tap(_ =>console.error('fetched characters')),
      catchError(this.handleError<Character[]>('getCharacters',[]))
    )
  } 


  getCharacter(id: number): Observable<Character>{//Permet de faire un get d'un Character pour afficher les détails de l'inventaire. Ceci est envoyé pour le Character-detailComponent.
    const url = `${this.characterUrl}/${id}`;
    return this.http.get<Character>(url).pipe(
      tap(_=> console.log(`fetcted character id=${id}`)),
      catchError(this.handleError<Character>(`getCharacter id = ${id}`))
    );
  }

  getPlayer(): Observable<Character>{//Permet de faire get le personnage du joueur.
    return this.http.get<Character>(this.playerUrl)
    .pipe(
      tap(_=>console.error('fetched player')),
      catchError(this.handleError<Character>(`getPlayer`))
    )
  }; 

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

}
