import { Injectable } from '@angular/core';
import { Item } from '../../Classes/Item';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private itemUrl = 'api/inventory';//Url de l'api.
  private handleError<T>(operation = 'operation', result?: T){
    return (error: any): Observable<T> =>{

      console.error(error);

      return of(result as T)
    }
  }

  constructor(
    private http: HttpClient
  ) { }

  getItems():Observable<Item[]>{//Fait un GEt sur le back pour l'inventaire. Ceci est envoyé pour InventoryComponent.
    return this.http.get<Item[]>(this.itemUrl)
      .pipe(
        tap(_=>console.error('fetched items')),
        catchError(this.handleError<Item[]>('getItems',[]))
      )
  }
  getItem(id: number): Observable<Item>{//Fait un Get sur le back pour l'item dont on veut les détail. Ceci est envoyé pour Item-detailComponent.
    const url = `${this.itemUrl}/${id}`;
    return this.http.get<Item>(url).pipe(
      tap(_=> console.log(`fetched item id = ${id}`)),
      catchError(this.handleError<Item>(`getItem id = ${id}`))
    )
  };



  addItem (item: Item): Observable<Item> {//Fonction pour indiquer au back qu'il faut ajouter un item à l'inventaire 
    return this.http.post<Item>(this.itemUrl, item, this.htppOption).pipe(
      tap(_=>console.log(`added item w/ id=${item.ID}`)),
      catchError(this.handleError<Item>('addItem'))
    );
  };

  deleteItem (item: Item | number): Observable<Item> {
    const id = typeof item === 'number' ? item : item.ID;

    const url = `${this.itemUrl}/${id}`;
  
    return this.http.delete<Item>(url, this.htppOption).pipe(
      tap(_ => console.log(`deleted item id=${id}`)),
      catchError(this.handleError<Item>('deleteItem'))
    );
  };

  htppOption = {
    headers: new HttpHeaders({ 'Content-Type':'application/json'})
  };
}
