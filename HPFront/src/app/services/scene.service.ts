import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Scene } from '../../Classes/Scene';

@Injectable({
  providedIn: 'root'
})
export class SceneService {

  private sceneUrl = 'http://localhost:4034/api/Scene';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin':'*',
      'Accept': 'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  private handleError<T>(operation = 'operation', result?: T){
    return (error: any): Observable<T> =>{

      console.error(error);

      return of(result as T)
    }
  }

  getScene(id: string): Observable<Scene>{
    const url = `${this.sceneUrl}/${id}`;
    
    return this.http.get<Scene>(url).pipe(
      tap( _ => console.log(`fetched scene id = ${id}` + _.Title)),
      catchError(this.handleError<Scene>(`getScene id = ${id}`))
    )
  };

}
