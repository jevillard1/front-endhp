import { Injectable } from '@angular/core';
import { Character } from '../../Classes/Character';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb(){
    const player = { id: 0, name: "toto", life: 100, mana:100, max_life: 100, max_mana: 100};
    const characters = [
      { id: 1, name: "Harry Potter", life: 100, mana: 100, max_life: 100, max_mana: 100},
      { id: 2, name: "Croc mou", life: 150, mana: 100, max_life: 150, max_mana: 100 },
      { id: 3, name: "Ron Weealay", life: 90, mana: 110, max_life:90, max_mana: 110 }
    ];
  
    const inventory = [
      {id : 1, name: "Clé", content:"Clés de la cave."},
      {id : 2, name: "Ballais volant", content: "Ballais d'un ancien champion de Quiditsh."},
      {id : 3, name: "Anneau", content: "Anneau rare ayant appartenut à quelle qu'un de connue"}
    ];

    const Scene = [
      {
        id: "1",
        contentPlain: "Il fait sombre ici {2 prendre la torche} ou {3 partir vite}?",
        content: "",
        title: "Le choix",
        listOfAction: []
      },
      {
        id: "2",
        contentPlain: "Vous prennez la torche mais quelque chose se rapproche ! Vite {3 prendre la fuite}?",
        content: "",
        title: "La torche",
        listOfAction: []
      },
      {
        id: "3",
        contentPlain: "Vous courrez aussi vite que possible ! {1 prendre la torche} ",
        content: "",
        title: "La fuite",
        listOfAction: []
      }
    ]
    return { player,  characters, inventory, Scene};
  }
  genId(characters: Character[]):number{
    return characters.length > 0? Math.max(...characters.map(characters => characters.id))+1 :1;
  }
}
