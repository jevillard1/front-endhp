import { Component, OnInit, HostListener } from '@angular/core';
import { Item } from './../../Classes/Item';
import { ItemService } from '../services/item.service';
/* import { trigger, state, style, animate, transition, keyframes } from '@angular/animations'; */


@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css'],
 /*  animations: [
    trigger('myInventory', [
      state('true', style({ background : '#008B8B',width: '150px', color:'white'})),
      state('false', style({ opacity: 0, width: '0px' })),
      transition('false <=> true', animate(500))
    ]),
  ]  */ 
})
export class InventoryComponent implements OnInit {

  items : Item[];//Variable contenant liste des items que possède le joueur.
  /* isShown = false; */
  selectItem: Item;//Variable contenant un item sélectionner pour afficher les détails de cette item.

  /* toggle():void{
    this.isShown = !this.isShown;
  } */
  constructor(
    private itemService: ItemService
  ) { }
  getItems():void{//Appel le back pour get les informations sur l'inventaire du joueur
    this.itemService.getItems().subscribe(items  =>this.items = items);
  }
  onSelect(item: Item):void{//Selectionne l'item de l'inventaire sur le quelle on veut les détails. Ceci est associé à un badge dans le HTML
    this.selectItem = item;
  }
  unSelect():void{//Permet de plus sélectionner l'item de l'inventaire choisie avant. Ceci est associé à un bouton dans le HMTL.
    this.selectItem = null;
  }
  /*
  getItem(id:number):void{
    this.itemService.getItem(id).subscribe(item => this.item = item);
  }*/
  ngOnInit() {
    this.getItems()
    
  }
  
  delete(item: Item): void {///Supprime un item de l'inventaire et signale le back que cette item n'appartient plus à l'inventaire.
    this.items = this.items.filter(i => i !== item);
    this.itemService.deleteItem(item).subscribe();
  }
  

  @HostListener('mouseenter') onMouseEnter(){//Réinitialise l'inventaire quand l'utilisateur rentre le curseur de la souris dans la zone de l'inventaire sur la page.
    this.getItems();
  }

  @HostListener('mouseleave') onMouseLeave(){//Réinitialise l'inventaire quand l'utilisateur sort le curseur de la souris dans la zone de l'inventaire sur la page.
    this.getItems();
  }
}
