import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Character } from 'src/Classes/Character';
import { ActivatedRoute } from '@angular/router';
import { CharacterService } from '../services/character.service';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.css']
})
export class CharacterDetailComponent implements OnInit {

  @Input() character: Character;//Personnage choisi pour afficher les détails.
  
  constructor(
    private route: ActivatedRoute,
    private location:Location,
    private characterService: CharacterService
  ) { }

  ngOnInit() {
    this.getCharacter();
  }
  getCharacter():void{//Appel le back pour avoir les information concernant le character en input.
    const id = +this.route.snapshot.paramMap.get('id');
    this.characterService.getCharacter(id)
      .subscribe(character => this.character = character);
  }
  
  goBack(): void{//Fonction pour revenir sur la page d'avant. Ceci est associé à un bouton dans le code HTML.
    this.location.back();
  }
}
