import { Action } from './Action';

export class Scene 
{
    ID: string;
    contentPlain: string;
    Content: string;
    Title: string;
}
