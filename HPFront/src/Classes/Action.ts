/*
*   Nom :
*
*   Fonction :
*
*   Attributs :
*       id      number  Représente l'id de la classe
*       name    string  Réprésente le noom
*
*/
export class Action {
    id: number;
    name: string;

    constructor( id: number, name: string )
    {
        this.id = id;
        this.name = name;
    }

}